﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dvd_rent.Web.Models
{
    public class MovieCopy
    {
        public int ID { get; set; }
        public int MovieId { get; set; }
        public string SerialNumber { get; set; }
        public DateTime BayDate { get; set; }
        public DateTime TrashDate { get; set; }
        public bool IsOnStock { get; set; }
    }
}